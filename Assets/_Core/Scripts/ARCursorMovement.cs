﻿using _Core.Scripts.Events;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARCursorMovement : MonoBehaviour
{
    private Camera _camera;
    private FixedJoint _fixedJoint;
    private bool _isSessionQualityOk;
    private bool _isFirstDetect = true;
    private bool _isFirstGrab = true;
    private Rigidbody _rigidbody;
    private MeshRenderer _meshRenderer;


    // Start is called before the first frame update
    void Start()
    {
        ARSession.stateChanged += HandleStateChanged;
    }

    private void HandleStateChanged(ARSessionStateChangedEventArgs obj)
    {
        _isSessionQualityOk = obj.state == ARSessionState.SessionTracking;
    }

    void OnEnable()
    {
        _camera = Camera.main;
        _fixedJoint = GetComponent<FixedJoint>();
        _rigidbody = GetComponent<Rigidbody>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (!GameStateManager.IsCursorAvailable())
            return;
        
        if (_isSessionQualityOk)
        {
            FollowPalmCenter();
        }
    }
    
    private void FollowPalmCenter()
    {
        HandInfo currentlyDetectedHand = ManomotionManager.Instance.Hand_infos[0].hand_info;
        ManoClass currentlyDetectedManoClass = currentlyDetectedHand.gesture_info.mano_class;
        Vector3 palmCenterPosition = currentlyDetectedHand.tracking_info.palm_center;

        if (currentlyDetectedManoClass == ManoClass.GRAB_GESTURE)
        {
            Vector3 targetPosition = ManoUtils.Instance.CalculateNewPosition(palmCenterPosition,
                currentlyDetectedHand.tracking_info.depth_estimation);
            // NOTE: Don't remove. This prevents jerkiness
            _rigidbody.MovePosition(Vector3.MoveTowards(_rigidbody.position, targetPosition, 
                20F * Time.fixedDeltaTime));

            if (_isFirstDetect)
            {
                EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_HAND_TO_GRAB));
                _isFirstDetect = false;
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Paddle"))
        {
            _fixedJoint.connectedBody = other.rigidbody;

            if (_isFirstGrab)
            {
                EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_GRAB_TO_GAME));
                _isFirstGrab = false;
                _meshRenderer.enabled = false;
            }
        }
    }

    private void OnCollisionExit(Collision other)
    {
        // if (other.gameObject.CompareTag("Paddle"))
        // {
        //     _isCollidingWithPaddle = false;
        //     _fixedJoint.connectedBody = null;
        // }
    }
}
