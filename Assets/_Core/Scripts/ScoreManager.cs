﻿using _Core.Scripts.Events;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private TextMeshPro scoreText;
    public int maxScore = 3;
    [SerializeField] public static int p1Score = 0;
    [SerializeField] public static int p2Score = 0;
    [SerializeField] private TextMeshProUGUI gameOverScoreTitleText;
    [SerializeField] private TextMeshProUGUI gameOverScoreText;

    void Start()
    {
        EventManager.Instance.AddListener<GameEvent>(HandleGameEvent);
    }

    private void HandleGameEvent(GameEvent e)
    {
        if (e.EventType.Equals(GameEvent.GameEventType.P1_SCORE))
        {
            p1Score += 1;
            scoreText.text = "<color=#67c50a>" + p1Score + "</color>" + " : <color=#ffd900>" + p2Score + "</color>";
        }

        if (e.EventType.Equals(GameEvent.GameEventType.P2_SCORE))
        {
            p2Score += 1;
            scoreText.text = "<color=#67c50a>" + p1Score + "</color>" + " : <color=#ffd900>" + p2Score + "</color>";
        }
        
        if ((e.EventType.Equals(GameEvent.GameEventType.P2_SCORE)
             || e.EventType.Equals(GameEvent.GameEventType.P1_SCORE))
            && p1Score >= maxScore || p2Score >= maxScore)
        {
            Debug.Log("Game over");
            gameOverScoreTitleText.text = (p1Score > p2Score) ? "YOU WON!" : "YOU LOST :(";
            gameOverScoreText.text = "SCORE:\n" + p1Score + " : " + p2Score;
            EventManager.Instance.Raise(new GameOverEvent());
        }
    }
}
