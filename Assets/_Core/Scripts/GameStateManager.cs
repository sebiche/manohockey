﻿using _Core.Scripts.Events;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    public static Difficulty CurrentDifficulty = Difficulty.EASY;

    public enum Difficulty
    {
        EASY, MEDIUM, HARD
    }

    public static void SelectEasy()
    {
        CurrentDifficulty = Difficulty.EASY;
        Debug.Log(CurrentDifficulty);
    }
    
    public static void SelectMedium()
    {
        CurrentDifficulty = Difficulty.MEDIUM;
        Debug.Log(CurrentDifficulty);
    }
    
    public static void SelectHard()
    {
        CurrentDifficulty = Difficulty.HARD;
        Debug.Log(CurrentDifficulty);
    }

    public enum GameState
    {
        // Main menu with Difficulty toggle & Start button
        MAIN_MENU,
        // Scanning the room for planes
        PLACEMENT_SCANNING,
        // Tapping to place the table
        PLACEMENT_TAP,
        // Scaling, rotating the table
        PLACEMENT_ADJUSTMENT,
        // Hand in front of camera prompt
        PLACEMENT_HAND,
        // Grab the paddle prompt
        PLACEMENT_GRAB,
        // In game
        IN_GAME,
        // Game over menu. (Options: Back to Main Menu (reload game), Restart)
        GAME_OVER
    }

    public static GameState CurrentGameState = GameState.MAIN_MENU;

    void OnEnable()
    {
        EventManager.Instance.AddListener<GameEvent>(HandleGameEvent);
        EventManager.Instance.AddListener<GameOverEvent>(HandleGameOverEvent);
    }

    private void HandleGameOverEvent(GameOverEvent e)
    {
        CurrentGameState = GameState.GAME_OVER;
    }

    private void HandleGameEvent(GameEvent e)
    {
        if (e.EventType.Equals(GameEvent.GameEventType.MAIN_MENU_TO_PLACEMENT_SCANNING))
        {
            CurrentGameState = GameState.PLACEMENT_SCANNING;
        }
        if (e.EventType.Equals(GameEvent.GameEventType.PLACEMENT_SCANNING_TO_TAP))
        {
            CurrentGameState = GameState.PLACEMENT_TAP;
        }
        if (e.EventType.Equals(GameEvent.GameEventType.PLACEMENT_TAP_TO_ADJUSTMENT))
        {
            CurrentGameState = GameState.PLACEMENT_ADJUSTMENT;
        }
        if (e.EventType.Equals(GameEvent.GameEventType.PLACEMENT_ADJUSTMENT_TO_HAND))
        {
            CurrentGameState = GameState.PLACEMENT_HAND;
        }
        if (e.EventType.Equals(GameEvent.GameEventType.PLACEMENT_HAND_TO_GRAB))
        {
            CurrentGameState = GameState.PLACEMENT_GRAB;
        }
        if (e.EventType.Equals(GameEvent.GameEventType.PLACEMENT_GRAB_TO_GAME))
        {
            CurrentGameState = GameState.IN_GAME;
        }
        if (e.EventType == GameEvent.GameEventType.SKIP_PLACEMENT)
        {
            CurrentGameState = GameState.IN_GAME;
        }
    }

    public void StartGame()
    {
        if (CurrentGameState == GameState.PLACEMENT_ADJUSTMENT)
        {
            CurrentGameState = GameState.IN_GAME;
        }
    }

    public static bool IsPlacementMode()
    {
        return CurrentGameState == GameState.PLACEMENT_SCANNING
               || CurrentGameState == GameState.PLACEMENT_ADJUSTMENT
               || CurrentGameState == GameState.PLACEMENT_TAP;
    }

    public static bool IsCursorAvailable()
    {
        return CurrentGameState == GameState.IN_GAME || CurrentGameState == GameState.PLACEMENT_HAND ||
               CurrentGameState == GameState.PLACEMENT_GRAB;
    }
}
