using _Core.Scripts.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Core.Scripts
{
    // Responsible for Button functions and Editor surrogate logic for MakeAppearOnPlane & ARCursorMovement
    public class ButtonController : MonoBehaviour
    {
        public bool SkipPlacementInEditor = true;

        public void MainMenuToPlacementScanning()
        {
            #if UNITY_EDITOR
                if (SkipPlacementInEditor)
                {
                    EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.SKIP_PLACEMENT));
                    return;
                }
            #endif
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.MAIN_MENU_TO_PLACEMENT_SCANNING));
        }

        private void EditorPlacementScanningToTap()
        {
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_SCANNING_TO_TAP));
        }

        public void PlacementTapToAdjustment()
        {
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_TAP_TO_ADJUSTMENT));
        }
        
        public void PlacementAdjustmentToHand()
        {
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_ADJUSTMENT_TO_HAND));
        }
        
        private void EditorPlacementHandToGrab()
        {
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_HAND_TO_GRAB));
        }

        public void GameOverToMainMenu()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }

        void Update()
        {
            #if UNITY_EDITOR
                if (GameStateManager.CurrentGameState == GameStateManager.GameState.PLACEMENT_SCANNING)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        EditorPlacementScanningToTap();
                    }
                }
                if (GameStateManager.CurrentGameState == GameStateManager.GameState.PLACEMENT_HAND)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        EditorPlacementHandToGrab();
                    }
                }
            #endif
        }
    }
}
