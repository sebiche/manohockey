﻿using System.Collections;
using System.Collections.Generic;
using _Core.Scripts.Events;
using UnityEngine;

public class AIPaddleController : MonoBehaviour
{
    [SerializeField] private Transform top, bottom, left, right;
    private float _yPosition;
    private float _xStart;
    private Rigidbody _rigidbody;
    [SerializeField] private GameObject _puck;
    public float MaxMovementSpeed = 15F;
    private Vector3 targetPosition = new Vector3();
    private bool isGameInProgress = false;

    private bool isFirstTimeInOpponentsHalf = true;
    private float offsetZFromTarget;    
    
    // TODO: Will probably have to refactor in terms of localPosition with everything nested in the table
    // Start is called before the first frame update
    void OnEnable()
    {
        _yPosition = transform.position.y;
        _xStart = transform.position.x;
        _rigidbody = GetComponent<Rigidbody>();
        EventManager.Instance.AddListener<GameEvent>(HandleGameEvent);
        EventManager.Instance.AddListener<GameOverEvent>(HandleGameOverEvent);
    }

    private void HandleGameEvent(GameEvent e)
    {
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_GRAB_TO_GAME)
        {
            isGameInProgress = true;
            if (GameStateManager.CurrentDifficulty == GameStateManager.Difficulty.EASY)
            {
                MaxMovementSpeed = 10F;
            }
            if (GameStateManager.CurrentDifficulty == GameStateManager.Difficulty.MEDIUM)
            {
                MaxMovementSpeed = 15F;
            }
            if (GameStateManager.CurrentDifficulty == GameStateManager.Difficulty.HARD)
            {
                MaxMovementSpeed = 20F;
            }
        }
    }

    private void HandleGameOverEvent(GameOverEvent e)
    {
        isGameInProgress = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isGameInProgress)
        {
            return;
        }
        
        float movementSpeed;

        // Z & X are free
        // if in opponent's half
        if (_puck.transform.position.x > bottom.transform.position.x)
        {
            if (isFirstTimeInOpponentsHalf)
            {
                isFirstTimeInOpponentsHalf = false;
                offsetZFromTarget = Random.Range(-0.2F, 0.2F);
            }
            
            movementSpeed = MaxMovementSpeed * Random.Range(0.1f, 0.3f);
            targetPosition = new Vector3(
                _xStart,
                transform.position.y,
                Mathf.Clamp(_puck.transform.position.z, left.position.z, right.position.z)
            );
        }
        else
        {
            isFirstTimeInOpponentsHalf = true;

            movementSpeed = Random.Range(MaxMovementSpeed * 0.4F, MaxMovementSpeed);
            targetPosition = new Vector3(
                Mathf.Clamp(_puck.transform.position.x, top.position.x, bottom.position.x),
                transform.position.y, 
                Mathf.Clamp(_puck.transform.position.z + offsetZFromTarget, left.position.z, right.position.z)
            );
        }
        
        _rigidbody.MovePosition(Vector3.MoveTowards(_rigidbody.position, targetPosition, movementSpeed * Time.fixedDeltaTime));
    }
}
