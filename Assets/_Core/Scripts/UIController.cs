﻿using _Core.Scripts.Events;
using UnityEngine;

// Responsible for displaying UI based on Game State transitions
public class UIController : MonoBehaviour
{
    [SerializeField] private Canvas mainMenuCanvas;
    [SerializeField] private Canvas loadingCanvas;
    [SerializeField] private Canvas placementScanningCanvas;
    [SerializeField] private Canvas placementTapCanvas;
    [SerializeField] private Canvas placementAdjustCanvas;
    [SerializeField] private Canvas placementHandCanvas;
    [SerializeField] private Canvas placementGrabCanvas;
    [SerializeField] private Canvas gameOverCanvas;

    void Start()
    {
        EventManager.Instance.AddListener<GameEvent>(HandleGameEvent);
        EventManager.Instance.AddListener<GameOverEvent>(HandleGameOverEvent);
    }

    private void HandleGameOverEvent(GameOverEvent e)
    {
        gameOverCanvas.gameObject.SetActive(true);
    }

    private void HandleGameEvent(GameEvent e)
    {
        if (e.EventType == GameEvent.GameEventType.MAIN_MENU_TO_PLACEMENT_SCANNING)
        {
            Debug.Log(e.EventType);
            mainMenuCanvas.gameObject.SetActive(false);
            placementScanningCanvas.gameObject.SetActive(true);
        }
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_SCANNING_TO_TAP)
        {
            Debug.Log(e.EventType);
            placementScanningCanvas.gameObject.SetActive(false);
            placementTapCanvas.gameObject.SetActive(true);
        }
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_TAP_TO_ADJUSTMENT)
        {
            Debug.Log(e.EventType);
            placementTapCanvas.gameObject.SetActive(false);
            placementAdjustCanvas.gameObject.SetActive(true);
        }
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_ADJUSTMENT_TO_HAND)
        {
            Debug.Log(e.EventType);
            placementAdjustCanvas.gameObject.SetActive(false);
            placementHandCanvas.gameObject.SetActive(true);
        }
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_HAND_TO_GRAB)
        {
            Debug.Log(e.EventType);
            placementHandCanvas.gameObject.SetActive(false);
            placementGrabCanvas.gameObject.SetActive(true);
        }
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_GRAB_TO_GAME)
        {
            Debug.Log(e.EventType);
            placementGrabCanvas.gameObject.SetActive(false);
        }

        if (e.EventType == GameEvent.GameEventType.SKIP_PLACEMENT)
        {
            Debug.Log(e.EventType);
            mainMenuCanvas.gameObject.SetActive(false);
        }
    }
}
