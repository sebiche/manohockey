﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckAudioManager : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip paddleCollision;
    [SerializeField] private AudioClip boundsCollision;
    [SerializeField] private AudioClip goal;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Goal1") || other.gameObject.CompareTag("Goal2"))
        {
            _audioSource.PlayOneShot(goal);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Paddle"))
        {
            _audioSource.PlayOneShot(paddleCollision);
        }
        if (other.gameObject.CompareTag("Bounds"))
        {
            _audioSource.PlayOneShot(boundsCollision);
        }
    }
}
