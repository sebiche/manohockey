﻿using _Core.Scripts.Events;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip gameMusic;
    [SerializeField] private AudioClip menuMusic;
    public float MenuMusicVolume = 0.7F;
    public float GameMusicVolume = 0.6F;


    // Start is called before the first frame update
    void OnEnable()
    {
        _audioSource = GetComponent<AudioSource>();
        EventManager.Instance.AddListener<GameEvent>(HandleGameEvent);
        EventManager.Instance.AddListener<GameOverEvent>(HandleGameOverEvent);
        _audioSource.Stop();
        _audioSource.clip = menuMusic;
        _audioSource.volume = MenuMusicVolume;
        _audioSource.Play();
    }

    private void HandleGameEvent(GameEvent e)
    {
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_GRAB_TO_GAME)
        {
            _audioSource.Stop();
            _audioSource.clip = gameMusic;
            _audioSource.volume = GameMusicVolume;
            _audioSource.Play();
        }
    }
    
    private void HandleGameOverEvent(GameOverEvent e)
    {
        _audioSource.Stop();
        _audioSource.clip = menuMusic;
        _audioSource.volume = MenuMusicVolume;
        _audioSource.Play();
    }
}
