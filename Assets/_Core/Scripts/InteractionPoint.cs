﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class InteractionPoint : MonoBehaviour
{
    private bool _isSessionQualityOk;

    
    // Start is called before the first frame update
    void Start()
    {
        ARSession.stateChanged += HandleStateChanged;
    }

    private void HandleStateChanged(ARSessionStateChangedEventArgs obj)
    {
        _isSessionQualityOk = obj.state == ARSessionState.SessionTracking;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isSessionQualityOk)
        {
            FollowPalmCenter();
        }
    }

    private void FollowPalmCenter()
    {
        HandInfo currentlyDetectedHand = ManomotionManager.Instance.Hand_infos[0].hand_info;
        ManoClass currentlyDetectedManoClass = currentlyDetectedHand.gesture_info.mano_class;

        Vector3 palmCenterPosition = currentlyDetectedHand.tracking_info.palm_center;

        if (currentlyDetectedManoClass == ManoClass.GRAB_GESTURE)
        {
            this.transform.position = ManoUtils.Instance.CalculateNewPosition(palmCenterPosition,
                currentlyDetectedHand.tracking_info.depth_estimation);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Interactable"))
        {
            Handheld.Vibrate();
            Destroy(other.gameObject);
        }
    }
}
