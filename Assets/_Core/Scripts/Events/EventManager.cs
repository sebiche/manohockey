using System;
using System.Collections.Generic;

namespace _Core.Scripts.Events
{
    public class EventManager
    {
        private static EventManager instance;

        public static EventManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventManager();
                }
                return instance;
            }
        }

        public delegate void EventDelegate<T>(T e) where T : Event;

        private delegate void EventDelegate(Event e);
        
        // The actual delegate. One delegate per unique event.
        // Each delegate has multiple invocation list items.
        private Dictionary<Type, EventDelegate> delegates = new Dictionary<Type, EventDelegate>();

        
        // Lookups only, one delegate lookup per listener
        private Dictionary<Delegate, EventDelegate> delegateLookup = new Dictionary<Delegate, EventDelegate>();

        public void AddListener<T>(EventDelegate<T> del) where T : Event
        {
            if (delegateLookup.ContainsKey(del))
            {
                return;
            }
            
            // Create non-generic delegate which calls generic delegate
            EventDelegate internalDelegate = (e) => del((T) e);
            delegateLookup[del] = internalDelegate;

            EventDelegate tempDel;
            if (delegates.TryGetValue(typeof(T), out tempDel))
            {
                delegates[typeof(T)] = tempDel += internalDelegate;
            }
            else
            {
                delegates[typeof(T)] = internalDelegate;
            }
        }
        
        public void RemoveListener<T>(EventDelegate<T> del) where T : Event
        {
            EventDelegate internalDelegate;
            if (delegateLookup.TryGetValue(del, out internalDelegate))
            {
                EventDelegate tempDel;
                if (delegates.TryGetValue(typeof(T), out tempDel))
                {
                    tempDel -= internalDelegate;
                    if (tempDel == null)
                    {
                        delegates.Remove(typeof(T));
                    }
                    else
                    {
                        delegates[typeof(T)] = tempDel;
                    }
                }

                delegateLookup.Remove(del);
            }
        }

        // used for debugging
        public int DelegateLookupCount
        {
            get { return delegateLookup.Count; }
        }

        // raise event to all listeners
        public void Raise(Event e)
        {
            EventDelegate del;
            if (delegates.TryGetValue(e.GetType(), out del))
            {
                del.Invoke(e);
            }
        }
        
        
    }
}
