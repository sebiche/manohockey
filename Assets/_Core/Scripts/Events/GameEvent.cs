﻿using Event = _Core.Scripts.Events.Event;

public class GameEvent : Event
{
    public GameEventType EventType;

    public enum GameEventType {
        // Triggers: Button
        // Listeners: placeOnPlane, UI controller, GameStateManager
        MAIN_MENU_TO_PLACEMENT_SCANNING,
        
        // Triggers: mobile - placeOnPlane, editor - tap 
        // Listeners: placeOnPlane, UI controller, GameStateManager
        PLACEMENT_SCANNING_TO_TAP,
        
        // Triggers: Button
        // Listeners: placeOnPlane, UI controller, GameStateManager
        PLACEMENT_TAP_TO_ADJUSTMENT,
        
        // Triggers: Button
        // Listeners: placeOnPlane, UI controller, GameStateManager, ARCursorMovement
        PLACEMENT_ADJUSTMENT_TO_HAND,

        // Triggers: mobile - ARCursorMovement, editor - tap
        // Listeners: UI controller, GameStateManager, ARCursorMovement
        PLACEMENT_HAND_TO_GRAB,
        
        // Triggers: mobile - ARCursorMovement, editor - CursorMovement
        // Listeners: UI controller, GameStateManager, ARCursorMovement
        PLACEMENT_GRAB_TO_GAME,

        P1_SCORE,
        P2_SCORE,

        SKIP_PLACEMENT
    }

    public GameEvent(GameEventType eventType)
    {
        EventType = eventType;
    }
}
