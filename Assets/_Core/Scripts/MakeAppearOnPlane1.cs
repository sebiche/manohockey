﻿using System.Collections.Generic;
using _Core.Scripts.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/// <summary>
/// Moves the ARSessionOrigin in such a way that it makes the given content appear to be
/// at a given location acquired via a raycast.
/// </summary>
[RequireComponent(typeof(ARSessionOrigin))]
[RequireComponent(typeof(ARRaycastManager))]
public class MakeAppearOnPlane1 : MonoBehaviour
{
    [SerializeField]
    [Tooltip("A transform which should be made to appear to be at the touch point.")]
    Transform m_Content;
    
    private ARPointCloudManager arPointCloudManager;
    private ARPlaneManager arPlaneManager;

    /// <summary>
    /// A transform which should be made to appear to be at the touch point.
    /// </summary>
    public Transform content
    {
        get { return m_Content; }
        set { m_Content = value; }
    }

    [SerializeField]
    [Tooltip("The rotation the content should appear to have.")]
    Quaternion m_Rotation;

    /// <summary>
    /// The rotation the content should appear to have.
    /// </summary>
    public Quaternion rotation
    {
        get { return m_Rotation; }
        set
        {
            m_Rotation = value;
            if (m_SessionOrigin != null)
                m_SessionOrigin.MakeContentAppearAt(content, content.transform.position, m_Rotation);
        }
    }

    void Awake()
    {
        m_SessionOrigin = GetComponent<ARSessionOrigin>();
        m_RaycastManager = GetComponent<ARRaycastManager>();
        arPlaneManager = GetComponent<ARPlaneManager>();
        arPointCloudManager = GetComponent<ARPointCloudManager>();
    }

    void OnEnable()
    {
        EventManager.Instance.AddListener<GameEvent>(HandleGameEvent);
    }

    private void HandleGameEvent(GameEvent e)
    {
        if (e.EventType == GameEvent.GameEventType.PLACEMENT_ADJUSTMENT_TO_HAND)
        {
            ClearAndDisablePlanesAndPoints();
        }
    }

    void Update()
    {
        if (!GameStateManager.IsPlacementMode())
            return;
        
        // Plane scanning -> Tap
        if (GameStateManager.CurrentGameState == GameStateManager.GameState.PLACEMENT_SCANNING)
        {
            if (arPlaneManager.trackables.count > 2)
            {
                EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_SCANNING_TO_TAP));
            }
        }

        // prevent clicks on buttons registering for place on plane
        if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.currentSelectedGameObject != null)
            return;
        if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            return;
        
        if (Input.touchCount == 0 || m_Content == null)
            return;

        var touch = Input.GetTouch(0);

        if (m_RaycastManager.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            // This does not move the content; instead, it moves and orients the ARSessionOrigin
            // such that the content appears to be at the raycast hit position.
            m_SessionOrigin.MakeContentAppearAt(content, hitPose.position, m_Rotation);
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_TAP_TO_ADJUSTMENT));
        }
    }
    
    public void ClearAndDisablePlanesAndPoints()
    { 
        SetManagerTrackables(false);
        arPlaneManager.enabled = false;
    }

    public void SetManagerTrackables(bool value)
    {
        foreach (var plane in arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(value);
        }
        
        foreach (var pointCloud in arPointCloudManager.trackables)
        {
            pointCloud.gameObject.SetActive(value);
        }
    }

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARSessionOrigin m_SessionOrigin;
    ARRaycastManager m_RaycastManager;
}
