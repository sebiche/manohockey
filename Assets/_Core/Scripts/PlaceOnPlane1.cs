using System.Collections.Generic;
using _Core.Scripts.Events;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/// <summary>
/// Listens for touch events and performs an AR raycast from the screen touch point.
/// AR raycasts will only hit detected trackables like feature points and planes.
///
/// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
/// and moved to the hit position.
/// </summary>
[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane1 : MonoBehaviour
{
    [SerializeField] private ARPointCloudManager arPointCloudManager;
    [SerializeField] private ARPlaneManager arPlaneManager;

    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    private bool hasBeenEnabled = false;

    void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;
            touchPosition = new Vector2(mousePosition.x, mousePosition.y);
            return true;
        }
#else
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
#endif

        touchPosition = default;
        return false;
    }
    

    void Update()
    {
        if (!GameStateManager.IsPlacementMode())
            return;

        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            if (!hasBeenEnabled)
            {
                placedPrefab.transform.position = hitPose.position;
                placedPrefab.SetActive(true);
                hasBeenEnabled = true;
                EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_TAP_TO_ADJUSTMENT));
            }
            else
            {
                placedPrefab.transform.position = hitPose.position;
            }
        }
    }

    public void ClearAndDisablePlanesAndPoints()
    { 
        SetManagerTrackables(false);
    }

    public void SetManagerTrackables(bool value)
    {
        foreach (var plane in arPlaneManager.trackables)
        {
            plane.gameObject.SetActive(value);
        }
        
        foreach (var pointCloud in arPointCloudManager.trackables)
        {
            pointCloud.gameObject.SetActive(value);
        }
    }

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
}
