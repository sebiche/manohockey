﻿using System.Collections;
using System.Collections.Generic;
using _Core.Scripts.Events;
using UnityEngine;

public class CursorMovement : MonoBehaviour
{
    private Camera _camera;
    private FixedJoint _fixedJoint;
    private bool isFirstGrab = true;

    // Start is called before the first frame update
    void OnEnable()
    {
        _camera = Camera.main;
        _fixedJoint = GetComponent<FixedJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameStateManager.IsCursorAvailable())
            return;
        
        // if clicking on paddle, attach joint
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out hitInfo))
            {
                // Debug.Log(hitInfo.collider.gameObject.name);
                if (hitInfo.collider.gameObject.CompareTag("Paddle"))
                {
                    // Debug.Log("Attached body");
                    _fixedJoint.connectedBody = hitInfo.collider.attachedRigidbody;

                    if (isFirstGrab)
                    {
                        EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.PLACEMENT_GRAB_TO_GAME));
                        isFirstGrab = false;
                    }
                }
            }
            else
            {
                // Debug.Log("no hit");
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            // Debug.Log("Detached body");
            _fixedJoint.connectedBody = null;
        }

        Vector2 mousePos = Input.mousePosition;
        transform.position = _camera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 7F));
        
        // TODO: ManoMotion version. Move cursor with hand. Raycast and attach on grab, detach on release.
    }
}
