﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class Clicker : MonoBehaviour
{
    private bool _isSessionQualityOk;

    public GameObject itemPrefab;

    void Start()
    {
        ARSession.stateChanged += HandleStateChanged;
    }

    private void HandleStateChanged(ARSessionStateChangedEventArgs obj)
    {
        _isSessionQualityOk = obj.state == ARSessionState.SessionTracking;
    }

    void Update()
    {
        if (_isSessionQualityOk && IsClickGesture())
        {
            SpawnCubeWithClickTriggerGesture();
        }
    }

    private bool IsClickGesture()
    {
        HandInfo handInformation = ManomotionManager.Instance.Hand_infos[0].hand_info;
        GestureInfo gestureInfo = handInformation.gesture_info;
        ManoGestureTrigger currentTriggerGesture = gestureInfo.mano_gesture_trigger;
        return currentTriggerGesture == ManoGestureTrigger.CLICK;
    }

    private void SpawnCubeWithClickTriggerGesture()
    {
        GameObject newItem = Instantiate(itemPrefab);
        Vector3 positionToMove = Camera.main.transform.position + Camera.main.transform.forward * 2;
        newItem.transform.position = positionToMove;
        Handheld.Vibrate();
    }
}
