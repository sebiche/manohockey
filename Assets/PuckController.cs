﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Core.Scripts.Events;
using UnityEngine;

public class PuckController : MonoBehaviour
{
    [SerializeField] private Transform start1;
    [SerializeField] private Transform start2;
    private Rigidbody _rigidbody;
    [SerializeField] private Transform wallTop, wallBottom, wallLeft, wallRight;
    private float _maxSpeed = 20F;
    
    // Start is called before the first frame update
    void OnEnable()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
    }

    // Update is called once per frame
    // -z left, +z right, -x up, +x down
    void Update()
    {
        bool outOfBounds = false;
        bool stuck = false;
        if (transform.position.z < wallLeft.position.z)
        {
            outOfBounds = true;
        }
        else if (transform.position.z > wallRight.position.z)
        {
            outOfBounds = true;
        }
        else if (transform.position.x < wallTop.position.x)
        {
            outOfBounds = true;
        }
        else if (transform.position.x > wallBottom.position.x)
        {
            outOfBounds = true;
        }

        if (_rigidbody.velocity.magnitude < .002F && _rigidbody.velocity.magnitude > 0)
        {
            stuck = true;
        }
        
        // if (_rigidbody)
        // if speed < 3e-05 && speed > 0
        
        // TODO: Display message
        if (outOfBounds)
        {
            Debug.Log("Puck out of bounds. Respawning");
            Respawn(start1.position);
        }

        // TODO: Only do this if on AI side
        if (stuck)
        {
            Debug.Log("Puck is stuck. Respawning");
            Respawn(start1.position);
        }
    }

    private void Respawn(Vector3 start)
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
        transform.position = start;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Goal1"))
        {
            Debug.Log("Goal1 hit");
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.P2_SCORE));
            Respawn(start2.position);
        }
        if (other.gameObject.CompareTag("Goal2"))
        {
            Debug.Log("Goal2 hit");
            EventManager.Instance.Raise(new GameEvent(GameEvent.GameEventType.P1_SCORE));
            Respawn(start1.position);
        }
    }
}
