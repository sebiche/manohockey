﻿using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class SessionStatusDisplayer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI sessionStatusText;

    // Start is called before the first frame update
    void Start()
    {
        ARSession.stateChanged += HandleStateChanged;
    }

    private void HandleStateChanged(ARSessionStateChangedEventArgs stateChangedEventArgs)
    {
        string status = "Status: " + stateChangedEventArgs.state.ToString();
        sessionStatusText.text = status;
    }
}
